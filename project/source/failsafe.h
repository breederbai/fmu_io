/**
  ******************************************************************************
  * @file       failsafe.h 
  * @brief      
  * @details    
  * @note       
  * @author     蒋朔
  * @date       2020-12-03
  * @copyright  北京宇系航通科技有限公司
  ******************************************************************************
  */

#ifndef FAILSAFE_H
#define FAILSAFE_H

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
/*-----------------------------------macro------------------------------------*/
/*----------------------------------typedef-----------------------------------*/

typedef struct {
	uint8_t start; //置位为开启丢失通信保护
	uint32_t period; //丢失通信时间,单位为ms
	uint32_t last_time; //上次通信时间,单位为ms
	uint32_t now_time; //当前时间
} failsafe_t;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void failsafe_init(uint32_t t);
void failsafe_update(void);
void failsafe_start(void);
void failsafe_stop(void);
uint8_t failsafe_check(void);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* FAILSAFE_H */



