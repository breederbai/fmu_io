/**
  ******************************************************************************
  * @file       failsafe.h 
  * @brief      
  * @details    
  * @note       
  * @author     蒋朔
  * @date       2020-12-03
  * @copyright  北京宇系航通科技有限公司
  ******************************************************************************
  */


/*----------------------------------include-----------------------------------*/

#include "time.h"
#include "failsafe.h"
#include "debug.h"

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static failsafe_t failsafe;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       通信丢失保护结构体初始化
  * @param[in]   t:未收到信号t毫秒后认为通信丢失
  * @param[out]  
  * @retval      
  * @note        
  */
void failsafe_init(uint32_t t)
{
    failsafe.start = 0;
    failsafe.period = t;
    failsafe.last_time = 0;
    failsafe.now_time = 0;
}

/**
  * @brief       开启保护
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
void failsafe_start(void)
{
    failsafe.start = 1;
    failsafe.last_time = time_nowMs();
}
/**
  * @brief       关闭保护
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
void failsafe_stop(void)
{
    failsafe.start = 0;
}


/**
  * @brief       更新上次通信时间
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
void failsafe_update(void)
{
    failsafe.last_time = time_nowMs();
}


/**
  * @brief       检查是否丢失通信
  * @param[in]   
  * @param[out]  
  * @retval      
  * @note        
  */
uint8_t failsafe_check(void)
{
    failsafe.now_time = time_nowMs();
    if(failsafe.start && failsafe.now_time > failsafe.last_time)
    {
        if(failsafe.now_time - failsafe.last_time >= failsafe.period)
        {
            return 1;
        }
    }
    return 0;
}

/*------------------------------------test------------------------------------*/

